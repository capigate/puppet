provider "aws" {
 access_key = "${var.access_key}"
 secret_key = "${var.secret_key}"
 region     = "us-east-1"
}

resource "aws_instance" "Puppet" {
  ami   = "ami-b63769a1"
  count = 1
  instance_type = "t2.micro"
  security_groups = ["sg-398c7d45"]
  subnet_id = "subnet-a320428e"
  source_dest_check = true
  key_name = "aws-linux-puppet"
  root_block_device {
    volume_size =  30
    volume_type =  "standard"
    delete_on_termination = "false"
  }
}
